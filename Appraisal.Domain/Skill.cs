﻿namespace Appraisal.Domain
{
    public class Skill
    {
        public int Id { get; set; }

        public string SkillName { get; set; }
        public int SkillCategoryId { get; set; }
        public SkillCategory SkillCategory { get; set; }
    }
}

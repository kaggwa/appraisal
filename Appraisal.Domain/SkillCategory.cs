﻿namespace Appraisal.Domain
{
    public class SkillCategory
    {
        public int Id { get; set; }
        public string SkillCategoryName { get; set; }
    }
}

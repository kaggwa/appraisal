﻿namespace Appraisal.Domain
{
    public class Rating
    {
        public int Id { get; set; }

        public string RatingName { get; set; }
    }
}

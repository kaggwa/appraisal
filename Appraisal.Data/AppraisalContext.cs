﻿using Appraisal.Domain;
using Microsoft.EntityFrameworkCore;

namespace Appraisal.Data
{
    public class AppraisalContext : DbContext
    {
        public DbSet<Skill> Skills { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source=(local);Initial Catalog=Appraisal;Integrated Security=True");
        }
    }


}
